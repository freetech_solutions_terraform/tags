output "id" {
  description = "Disambiguated ID"
  value       = local.id
}

output "name" {
  description = "Normalized name"
  value       = local.name
}

output "role" {
  description = "Normalized role"
  value       = local.role
}

output "prefix" {
  description = "Normalized prefix"
  value       = local.prefix
}

output "environment" {
  description = "Normalized environment"
  value       = local.environment
}

output "costCenter" {
  description = "Normalized costCenter"
  value       = local.costCenter
}

output "tagVersion" {
  description = "Normalized tagVersion"
  value       = local.tagVersion
}

output "owner" {
  description = "Normalized owner"
  value       = local.owner
}

output "project" {
  description = "Normalized Project"
  value       = local.project
}

# Merge input tags with our tags
output "tags" {
  description = "Normalized Tag map"

  value = merge(
    var.tags,
    {
      "Name"        = local.id
      "prefix"      = local.prefix
      "environment" = local.environment
      "role"        = local.role
      "costCenter"  = local.costCenter
      "tagVersion"  = local.tagVersion
      "project"     = local.project
      "owner"       = local.owner
    },
  )
}
