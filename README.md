# tf-null-label

Terraform module designed to generate consistent label names and tags for resources. Use `tf-null-label` to implement a strict naming convention.

A label follows the following convention: `{prefix}-{role}`. The delimiter (e.g. `-`) is interchangeable and the convention can be overwritten by the parameter `name`.

If you have multiple different kinds of resources (e.g. instances, security groups, file systems, and elastic ips), then they can all share the same label assuming they are logically related.

**NOTES:**
  
  * The previous version of this module supports mandatory tags defined in "tags" section or defined in variables. This version ONLY supports mandatory tags as variables. 
  * The `null` refers to the primary Terraform [provider](https://www.terraform.io/docs/providers/null/index.html) used in this module. The logic is now replaced by [locals](https://www.terraform.io/docs/configuration/locals.html) but we kept the module name.
  * This module has been inspired by [CloudPosse tf-null-label](https://github.com/cloudposse/terraform-null-label)

## Requirements

  * Terraform >= 0.12

## Usage

The mandatory tags must be passed as variable. It is possible add more "non mandatory" tags using the "tags" section.

```hcl
module "label_name" {
  source = "../../modules/tf-null-label"

  name        = "bastion-instance"
  prefix      = "stg"
  environment = "staging"
  role        = "bastion"
  costCenter  = "CloudEng"
  owner       = "CloudEng"
  project     = "cloud-governance"

  tags   = {
    finance_region        = "us-east-1"
    finance_costcenter_id = "costcenter"
  }
```

The module would return the following map
```
module.label_name.id = bastion-instance

module.label_role.tags = {
  Name                  = "bastion-instance"
  prefix                = "stg"
  environment           = "staging"
  role                  = "bastion"
  costCenter            = "CloudEng"
  owner                 = "CloudEng"
  project               = "cloud-governance"
  tagVersion            = "1"  
  finance_region        = "us-east-1"
  finance_costcenter_id = "costcenter"
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| costCenter | Cost Allocation of the resource | string | n/a | yes |
| delimiter | Delimiter to be used between `prefix` and `role` | string | `"-"` | no |
| environment | Name of the environment you are working on (development, staging, production) | string | n/a | yes |
| name | Name, overwrites the default convention | string | `""` | no |
| owner | Username, email or team | string | n/a | yes |
| prefix | Prefix that identifies your environment (dev, stg, pro) | string | `""` | no |
| project | Project of the resource | string | n/a | yes |
| role | Role, which is the application name (jenkins, openshift) | string | n/a | yes |
| tagVersion | Version of the tagging standard used | string | `"1"` | no |
| tags | A map with additional tags (`prefix` and `role` are added to the tags map) | map | `<map>` | no |

## Outputs

| Name | Description |
|------|-------------|
| costCenter | Normalized costCenter |
| environment | Normalized environment |
| id | Disambiguated ID |
| name | Normalized name |
| owner | Normalized owner |
| prefix | Normalized prefix |
| project | Normalized Project |
| role | Normalized role |
| tagVersion | Normalized tagVersion |
| tags | Normalized Tag map |
