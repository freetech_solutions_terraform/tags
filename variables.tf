variable "prefix" {
  description = "Prefix that identifies your environment (dev, stg, pro)"
  type        = string
  default     = ""
}

variable "environment" {
  description = "Name of the environment you are working on (development, staging, production)"
  type        = string
}

variable "role" {
  description = "Role, which is the application name (jenkins, openshift)"
  type        = string
}

variable "name" {
  description = "Name, overwrites the default convention"
  default     = ""
  type        = string
}

variable "costCenter" {
  description = "Cost Allocation of the resource"
  type        = string
}

variable "tagVersion" {
  description = "Version of the tagging standard used"
  default     = "1"
}

variable "owner" {
  description = "Username, email or team"
  type        = string
}

variable "project" {
  description = "Project of the resource"
  type        = string
}

variable "delimiter" {
  description = "Delimiter to be used between `prefix` and `role`"
  type        = string
  default     = "-"
}

variable "tags" {
  description = "A map with additional tags (`prefix` and `role` are added to the tags map)"
  type        = map(string)
  default     = {}
}
