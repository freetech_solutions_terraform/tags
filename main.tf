locals {
  name        = lower(var.name)
  prefix      = lower(var.prefix)
  environment = lower(var.environment)
  role        = lower(var.role)
  costCenter  = lower(var.costCenter)
  tagVersion  = lower(var.tagVersion)
  owner       = lower(var.owner)
  project     = lower(var.project)
  id = lower(
    local.name == "" ? join(var.delimiter, compact([local.prefix, local.role])) : local.name
  )
}
